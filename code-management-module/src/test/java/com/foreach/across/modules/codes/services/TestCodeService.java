package com.foreach.across.modules.codes.services;

import com.foreach.across.modules.codes.domain.code.*;
import com.foreach.across.modules.codes.utils.CodeGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestCodeService
{
	@Mock
	private CodeRepository codeRepository;

	@Mock
	private CodeBatchRepository codeBatchRepository;

	@Mock
	private CodeUsageRepository codeUsageRepository;

	@InjectMocks
	private CodeServiceImpl codeCreationService;

	private CodeBatch dummyCodeBatch;

	private Code dummyCode;

	@Before
	public void setup() {
		dummyCodeBatch = new CodeBatch();
		dummyCodeBatch.setEnabled( true );
		dummyCodeBatch.setName( "CodeGroup" );

		dummyCode = new Code();
		dummyCode.setCodeBatch( dummyCodeBatch );
		dummyCode.setValue( "dummy" );
	}

	@Test
	public void codeGenerator() {
		when( codeRepository.save( any( Code.class ) ) ).thenReturn( any() );

		codeCreationService.generateUniqueCodesBatch( dummyCodeBatch, 10, "{10}" );

		verify( codeRepository, times( 10 ) ).save( any( Code.class ) );
		verify( codeBatchRepository, times( 1 ) ).save( any( CodeBatch.class ) );
	}

	@Test
	public void useCode(){
		codeCreationService.useCode( dummyCode );

		verify( codeUsageRepository, times(1)).save( any(CodeUsage.class ) );
	}
}
