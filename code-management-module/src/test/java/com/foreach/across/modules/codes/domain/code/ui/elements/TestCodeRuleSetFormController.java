package com.foreach.across.modules.codes.domain.code.ui.elements;

import com.foreach.across.modules.codes.domain.code.CodeGroup;
import com.foreach.across.modules.codes.domain.code.CodeGroupRepository;
import com.foreach.across.modules.codes.domain.code.CodeRuleSetRepository;
import com.foreach.across.modules.codes.domain.code.CodeRuleSetService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.SmartValidator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestCodeRuleSetFormController
{
	@Mock
	@SuppressWarnings( "unused" )
	private SmartValidator entityValidator;

	@Mock
	private CodeRuleSetRepository codeRuleSetRepository;

	@Mock
	private CodeGroupRepository codeGroupRepository;

	@Mock
	private CodeRuleSetService codeRuleSetService;

	@InjectMocks
	private CodeRuleSetFormController controller;

	private CodeGroup group = new CodeGroup();
	private CodeRuleSetForm form = CodeRuleSetForm.from( group );

	@Test
	public void aNewInheritedRuleSetIsNotAssigned() {
		assertThat( form.isInherited() ).isTrue();
		assertThat( form.getRuleSet().isNew() ).isTrue();

		controller.save( group, form );

		assertThat( group.getCodeRuleSet() ).isNull();
		verifyZeroInteractions( codeRuleSetRepository, codeGroupRepository );
	}

	@Test
	public void aNewRuleSetIsAssignedIfNotInherited() {
		form.setInherited( false );
		assertThat( form.getRuleSet().isNew() ).isTrue();

		controller.save( group, form );

		InOrder inOrder = inOrder( codeGroupRepository, codeRuleSetRepository );
		inOrder.verify( codeRuleSetRepository ).save( form.getRuleSet() );
		inOrder.verify( codeGroupRepository ).save( group );

		assertThat( group.getCodeRuleSet() ).isNotNull().isSameAs( form.getRuleSet() );
		verifyZeroInteractions( codeRuleSetService );
	}

	@Test
	public void existingRuleSetIsRemovedCompletelyIfNoOtherLinks() {
		form.getRuleSet().setId( 123L );

		assertThat( form.isInherited() ).isTrue();
		assertThat( form.getRuleSet().isNew() ).isFalse();

		controller.save( group, form );

		InOrder inOrder = inOrder( codeGroupRepository, codeRuleSetService );
		inOrder.verify( codeGroupRepository ).save( group );
		inOrder.verify( codeRuleSetService ).deleteRuleSetIfPossible( form.getRuleSet() );
		verifyZeroInteractions( codeGroupRepository, codeRuleSetRepository );

		assertThat( group.getCodeRuleSet() ).isNull();
	}
}
