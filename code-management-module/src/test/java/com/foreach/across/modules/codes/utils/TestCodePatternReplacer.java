package com.foreach.across.modules.codes.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Test;

import java.util.Iterator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class TestCodePatternReplacer
{

	@Test
	public void patternsAreReplaced() {
		String interFixPattern = "-" + RandomStringUtils.random( 3 ) + "-";
		String suffixPattern = "-" + RandomStringUtils.random( 5 );
		CodePatternFormatter formatter = CodePatternFormatter.forPattern( "{4}" + interFixPattern + "{10}" + suffixPattern );
		int codeLength = 14;
		int extraPatternLength = suffixPattern.length() + interFixPattern.length();
		CodeGenerator generator = CodeGenerator.forCodeLength( codeLength );
		Iterator<String> it = generator.generate( 3 );
		while ( it.hasNext() ) {
			String code = it.next();
			code = formatter.replace( code );
			assertThat( code ).contains( interFixPattern ).endsWith( suffixPattern ).hasSize( codeLength + extraPatternLength );
		}
		generator.generate( 3 ).forEachRemaining( code -> {
			code = formatter.replace( code );
			assertThat( code ).contains( interFixPattern ).endsWith( suffixPattern ).hasSize( codeLength + extraPatternLength );
		} );
		assertThat( formatter.getCodeLength() ).isEqualTo( codeLength );
	}

	@Test
	public void validateInvalidPatterns() {
		assertThatThrownBy( () -> CodePatternFormatter.forPattern( "" ) ).hasMessage( "Invalid pattern: \"\". Must be in format ABC-{14}-XYZ." );
		String random = RandomStringUtils.randomAlphanumeric( 50 );
		assertThatThrownBy( () -> CodePatternFormatter.forPattern( random ) ).hasMessage(
				"Invalid pattern: \"" + random + "\". Must be in format ABC-{14}-XYZ." );
		assertThatThrownBy( () -> CodePatternFormatter.forPattern( "{}" ) ).hasMessage( "Invalid pattern: \"{}\". Must be in format ABC-{14}-XYZ." );
		assertThatThrownBy( () -> CodePatternFormatter.forPattern( "{15d}" ) ).hasMessage( "Invalid pattern: \"{15d}\". Must be in format ABC-{14}-XYZ." );
		String crazyNumberProvided = RandomStringUtils.randomNumeric( 1000 );
		NumberUtils.isParsable( crazyNumberProvided );
		assertThatThrownBy( () -> CodePatternFormatter.forPattern( "{" + crazyNumberProvided + "}" ) ).hasMessage(
				"The pattern argument must be less than: \"2147483647\"." );
		assertThatThrownBy( () -> CodePatternFormatter.forPattern( "{" + Integer.MAX_VALUE + "}" ) ).hasMessage(
				"The total length of codes cannot exceed 10000. The current code length for pattern \"{2147483647}\" is \"2147483647\"." );
		assertThatThrownBy( () -> CodeGenerator.forCodeLength( CodePatternFormatter.forPattern( "{39}-{9999}" ).getCodeLength() ) ).hasMessage(
				"The total length of codes cannot exceed 10000. The current code length for pattern \"{39}-{9999}\" is \"10038\"." );
	}
}
