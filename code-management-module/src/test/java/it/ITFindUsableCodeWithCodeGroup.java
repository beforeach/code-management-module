package it;

import com.foreach.across.modules.codes.domain.code.Code;
import org.junit.Test;

public class ITFindUsableCodeWithCodeGroup extends AbstractCodeFinderTest
{

	@Test
	public void nothingFoundForInvalidCode() {
		Code code = codeService.findUsableCode( "invalidCode", codeGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void findUsableCodeByCode() {
		Code code = codeService.findUsableCode( "AvailableCode", codeGroup );
		assertCodeValue( code, "AvailableCode" );
	}

	@Test
	public void whenCodeIsAlreadyUsed() {
		Code code = codeService.findUsableCode( "UsedCode", codeGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void withDisabledCodeBatch() {
		Code code = codeService.findUsableCode( "disabled", codeGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void withDisabledCodeGroup() {
		Code code = codeService.findUsableCode( "disabledCodeGroup", codeGroupDisabled );
		assertNoCodeValue( code );
	}

	@Test
	public void withStartDateOnCode() {
		Code code = codeService.findUsableCode( "codeInTheFuture", codeGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void withFutureStartDateOnCodeBatch() {
		Code code = codeService.findUsableCode( "futureCodeBatchCode", codeGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void withStartDateOnCodeGroup() {
		Code code = codeService.findUsableCode( "futureCodeGroup", futureCodeGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void withPassedEndDateOnCode() {
		Code code = codeService.findUsableCode( "CodeWithEndDate", codeGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void withEndDateOnCodeBatch() {
		Code code = codeService.findUsableCode( "endDatePassedCodeGroup", codeGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void withEndDateOnCodeGroup() {
		Code code = codeService.findUsableCode( "endedCodeGroup", endedCodeGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void findCodeByCodeWithTwoOuOfTThreeUsedCodes() {
		Code code = codeService.findUsableCode( "UsedCodeTwice", codeGroup );
		assertCodeValue( code, "UsedCodeTwice" );
	}

	@Test
	public void canNotFindCodeByCodeWithThreeOuOfThreeUsedCodes() {
		Code code = codeService.findUsableCode( "CodeUsedThreeTimes", codeGroup );
		assertNoCodeValue( code );
	}

	//TODO: Should be implemented
//	@Test
//	public void canNotFindCodeWithNumberOfCodesThatCanBeUsedReached() {
//		Code code = codeService.findUsableCode( oneOfTheGeneratedCodesThatIsNotUsed.getValue(), codeGroup );
//		assertNoCodeValue( code );
//
//		Code code2 = codeService.findUsableCode( oneOfTheGeneratedCodesThatIsUsed.getValue(), codeGroup );
//		assertNoCodeValue( code2 );
//	}
}
