package it;

import com.foreach.across.modules.codes.CodeManagementModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.test.AcrossTestConfiguration;
import com.foreach.across.test.AcrossWebAppConfiguration;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Bootstraps simple application with CodeManagementModule.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@AcrossWebAppConfiguration(classes = { AbstractCmsApplicationIT.Config.class })
public abstract class AbstractCmsApplicationIT
{
	@AcrossTestConfiguration(modules = { CodeManagementModule.NAME, AcrossHibernateJpaModule.NAME })
	protected static class Config extends DynamicDataSourceConfigurer
	{
	}
}

