package it;

import com.foreach.across.modules.codes.domain.code.Code;
import org.junit.Test;

public class ITFindUsableCodeByCode extends AbstractCodeFinderTest
{

	@Test
	public void nothingFoundForInvalidCode() {
		Code code = codeService.findUsableCode( "invalidCode" );
		assertNoCodeValue( code );
	}

	@Test
	public void findUsableCodeByCode() {
		Code code = codeService.findUsableCode( "AvailableCode" );
		assertCodeValue( code, "AvailableCode" );
	}

	@Test
	public void whenCodeIsAlreadyUsed() {
		Code code = codeService.findUsableCode( "UsedCode" );
		assertNoCodeValue( code );
	}

	@Test
	public void withDisabledCodeBatch() {
		Code code = codeService.findUsableCode( "disabled" );
		assertNoCodeValue( code );
	}

	@Test
	public void withDisabledCodeGroup() {
		Code code = codeService.findUsableCode( "disabledCodeGroup" );
		assertNoCodeValue( code );
	}

	@Test
	public void withStartDateOnCode() {
		Code code = codeService.findUsableCode( "codeInTheFuture" );
		assertNoCodeValue( code );
	}

	@Test
	public void withFutureStartDateOnCodeBatch() {
		Code code = codeService.findUsableCode( "futureCodeGroup" );
		assertNoCodeValue( code );
	}

	@Test
	public void withStartDateOnCodeGroup() {
		Code code = codeService.findUsableCode( "futureCodeGroup" );
		assertNoCodeValue( code );
	}

	@Test
	public void withPassedEndDateOnCode() {
		Code code = codeService.findUsableCode( "TomWithEndDate" );
		assertNoCodeValue( code );
	}

	@Test
	public void withEndDateOnCodeBatch() {
		Code code = codeService.findUsableCode( "codeWithPassedEndDateCodeBatch" );
		assertNoCodeValue( code );
	}

	@Test
	public void withEndDateOnCodeGroup() {
		Code code = codeService.findUsableCode( "endedCodeGroup" );
		assertNoCodeValue( code );
	}

	@Test
	public void findCodeByCodeWithTwoOuOfTThreeUsedCodes() {
		Code code = codeService.findUsableCode( "UsedCodeTwice" );
		assertCodeValue( code, "UsedCodeTwice" );
	}

	@Test
	public void canNotFindCodeByCodeWithThreeOuOfThreeUsedCodes() {
		Code code = codeService.findUsableCode( "CodeUsedThreeTimes" );
		assertNoCodeValue( code );
	}

	//TODO: Should be implemented
//	@Test
//	public void canNotFindCodeWithNumberOfCodesThatCanBeUsedReached() {
//		Code code = codeService.findUsableCode( oneOfTheGeneratedCodesThatIsNotUsed.getValue() );
//		assertNoCodeValue( code );
//
//		Code code2 = codeService.findUsableCode( oneOfTheGeneratedCodesThatIsUsed.getValue() );
//		assertNoCodeValue( code2 );
//	}
}
