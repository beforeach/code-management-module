package it;

import com.foreach.across.modules.codes.domain.code.*;
import com.foreach.across.modules.codes.services.CodeService;
import com.foreach.across.modules.codes.utils.CodeGenerator;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

abstract public class AbstractCodeFinderTest extends AbstractCmsApplicationIT
{
	private static Boolean dataSet = false;

	@Autowired
	protected CodeService codeService;
	@Autowired
	private CodeBatchRepository codeBatchRepository;
	@Autowired
	private CodeRepository codeRepository;
	@Autowired
	private CodeGroupRepository codeGroupRepository;
	@Autowired
	private CodeRuleSetRepository codeRuleSetRepository;
	@Autowired
	private CodeUsageRepository codeUsageRepository;

	protected static CodeGroup codeGroup;
	protected static CodeGroup codeGroupDisabled;
	protected static CodeGroup endedCodeGroup;
	protected static CodeGroup futureCodeGroup;

	protected static CodeRuleSet passedEndDateCodeRuleSetForCodeGroup;
	protected static CodeRuleSet threeUsageRuleSet;
	protected static CodeRuleSet defaultCodeRuleSet;
	protected static CodeRuleSet rightTimeRuleSet;
	protected static CodeRuleSet passedEndDateCodeRuleSet;
	protected static CodeRuleSet futureCodeSet;

	protected static CodeBatch codeBatchOne;
	protected static CodeBatch codeBatchTwo;
	protected static CodeBatch disabledCodeBatch;
	protected static CodeBatch futureCodeBatch;
	protected static CodeBatch passedEndDateCodeBatch;
	protected static CodeBatch codeBatchForDisabledCodeGroup;
	protected static CodeBatch batchForEndedGroup;
	protected static CodeBatch codeBatchForFutureGroup;

	protected static Code oneOfTheGeneratedCodesThatIsNotUsed;
	protected static Code oneOfTheGeneratedCodesThatIsUsed;

	@Before
	public void setupData() {
		if ( !dataSet ) {
			createCodeGroups();
			createDefaultCodeRuleSets();
			createCodeBatches();
			createAndAssignCodes();
			dataSet = true;
		}
	}

	private void createAndAssignCodes() {
		List<Code> codesForBatchOne = new ArrayList<Code>();
		Iterator<String> codesForBatchOneIterator = CodeGenerator.forCodeLength( 5 ).generate( 10 );
		while ( codesForBatchOneIterator.hasNext() ) {
			String codeValue = codesForBatchOneIterator.next();
			Code code = new Code();
			code.setValue( "batchOne" + codeValue );
			code.setCodeBatch( codeBatchOne );
			codesForBatchOne.add( code );
		}

		Code availableCode = new Code();
		availableCode.setValue( "AvailableCode" );
		availableCode.setCodeBatch( codeBatchTwo );

		Code codeForFuture = new Code();
		codeForFuture.setValue( "codeInTheFuture" );
		codeForFuture.setCodeBatch( codeBatchTwo );

		Code codeWithPassedEndDate = new Code();
		codeWithPassedEndDate.setValue( "CodeWithEndDate" );
		codeWithPassedEndDate.setCodeBatch( codeBatchTwo );

		Code codeThatIsUsedThreeTimes = new Code();
		codeThatIsUsedThreeTimes.setValue( "CodeUsedThreeTimes" );
		codeThatIsUsedThreeTimes.setCodeBatch( codeBatchTwo );

		Code usedCode = new Code();
		usedCode.setValue( "UsedCode" );
		usedCode.setCodeBatch( codeBatchTwo );

		Code codeThatIsUsedTwice = new Code();
		codeThatIsUsedTwice.setValue( "UsedCodeTwice" );
		codeThatIsUsedTwice.setCodeBatch( codeBatchTwo );

		Code codeInFutureGroup = new Code();
		codeInFutureGroup.setValue( "futureCodeGroup" );
		codeInFutureGroup.setCodeBatch( codeBatchForFutureGroup );

		List<Code> codesForBatchTwo = new ArrayList<>();
		codesForBatchTwo.add( availableCode );
		codesForBatchTwo.add( codeForFuture );
		codesForBatchTwo.add( codeWithPassedEndDate );
		codesForBatchTwo.add( codeThatIsUsedThreeTimes );
		codesForBatchTwo.add( usedCode );
		codesForBatchTwo.add( codeThatIsUsedTwice );
		codesForBatchTwo.add( codeInFutureGroup );

		Code codeForDisabledCodeBatch = new Code();
		codeForDisabledCodeBatch.setCodeBatch( disabledCodeBatch );
		codeForDisabledCodeBatch.setValue( "disabled" );
		codeRepository.save( codeForDisabledCodeBatch );

		Code codeForDisabledCodeGroup = new Code();
		codeForDisabledCodeGroup.setCodeBatch( codeBatchForDisabledCodeGroup );
		codeForDisabledCodeGroup.setValue( "disabledCodeGroup" );
		codeRepository.save( codeForDisabledCodeGroup );

		Code codeForEndedCodeGroup = new Code();
		codeForEndedCodeGroup.setCodeBatch( batchForEndedGroup );
		codeForEndedCodeGroup.setValue( "endedCodeGroup" );
		codeForEndedCodeGroup.setCodeRuleSet( passedEndDateCodeRuleSet );
		codeRepository.save( codeForEndedCodeGroup );

		Code codeWithFutureCodeBatch = new Code();
		codeWithFutureCodeBatch.setCodeBatch( futureCodeBatch );
		codeWithFutureCodeBatch.setValue( "futureCodeBatchCode" );
		codeRepository.save( codeWithFutureCodeBatch );

		Code codeWithPassedEndDateCodeBatch = new Code();
		codeWithPassedEndDateCodeBatch.setCodeBatch( passedEndDateCodeBatch );
		codeWithPassedEndDateCodeBatch.setValue( "endDatePassedCodeGroup" );
		codeRepository.save( codeWithPassedEndDateCodeBatch );

		codeBatchOne.setCodeRuleSet( defaultCodeRuleSet );
		codeBatchRepository.save( codeBatchOne );

		endedCodeGroup.setCodeRuleSet( passedEndDateCodeRuleSetForCodeGroup );
		codeGroupRepository.save( endedCodeGroup );

		futureCodeGroup.setCodeRuleSet( futureCodeSet );
		codeGroupRepository.save( futureCodeGroup );

		codeForFuture.setCodeRuleSet( futureCodeSet );
		availableCode.setCodeRuleSet( rightTimeRuleSet );
		codeWithPassedEndDate.setCodeRuleSet( passedEndDateCodeRuleSet );

		codeBatchTwo.setCodeRuleSet( defaultCodeRuleSet );
		codeBatchRepository.save( codeBatchTwo );

		codeThatIsUsedTwice.setCodeRuleSet( threeUsageRuleSet );
		codeRepository.save( codeThatIsUsedTwice );

		codeRepository.save( codesForBatchOne );
		codeRepository.save( codesForBatchTwo );

		// Test with single usage
		CodeUsage codeUsage = new CodeUsage();
		codeUsage.setCode( usedCode );
		codeUsage.setUsageDate( LocalDateTime.now() );
		codeUsageRepository.save( codeUsage );

		// Test with 3 usages of 3
		CodeUsage codeUsageOne = new CodeUsage();
		codeUsageOne.setCode( codeThatIsUsedThreeTimes );
		codeUsageOne.setUsageDate( LocalDateTime.now() );
		codeUsageRepository.save( codeUsageOne );
		CodeUsage codeUsageTwo = new CodeUsage();
		codeUsageTwo.setCode( codeThatIsUsedThreeTimes );
		codeUsageTwo.setUsageDate( LocalDateTime.now() );
		codeUsageRepository.save( codeUsageTwo );
		CodeUsage codeUsageThree = new CodeUsage();
		codeUsageThree.setCode( codeThatIsUsedThreeTimes );
		codeUsageThree.setUsageDate( LocalDateTime.now() );
		codeUsageRepository.save( codeUsageThree );

		// Test with 2 usages of 3
		CodeUsage codeUsageTwoForTwoUsageCode = new CodeUsage();
		codeUsageTwoForTwoUsageCode.setCode( codeThatIsUsedTwice );
		codeUsageTwoForTwoUsageCode.setUsageDate( LocalDateTime.now() );
		codeUsageRepository.save( codeUsageTwoForTwoUsageCode );

		CodeUsage codeUsageThreeForTwoUsageCode = new CodeUsage();
		codeUsageThreeForTwoUsageCode.setCode( codeThatIsUsedTwice );
		codeUsageThreeForTwoUsageCode.setUsageDate( LocalDateTime.now() );
		codeUsageRepository.save( codeUsageThreeForTwoUsageCode );

		codesForBatchOne.stream()
		                .limit( 4 )
		                .forEach( code -> {
			                CodeUsage codeUsageItem = new CodeUsage();
			                codeUsageItem.setCode( code );
			                codeUsageItem.setUsageDate( LocalDateTime.now() );
			                codeUsageRepository.save( codeUsageItem );
		                } );

		oneOfTheGeneratedCodesThatIsUsed = codesForBatchOne.get( 1 );
		oneOfTheGeneratedCodesThatIsNotUsed = codesForBatchOne.get( 6 );
	}

	private void createCodeBatches() {
		codeBatchOne = new CodeBatch();
		codeBatchOne.setEnabled( true );
		codeBatchOne.setName( "Code batch one" );
		codeBatchOne.setCodeGroup( codeGroup );
		codeBatchRepository.save( codeBatchOne );

		codeBatchTwo = new CodeBatch();
		codeBatchTwo.setEnabled( true );
		codeBatchTwo.setName( "Code batch two" );
		codeBatchTwo.setCodeGroup( codeGroup );
		codeBatchRepository.save( codeBatchTwo );

		disabledCodeBatch = new CodeBatch();
		disabledCodeBatch.setEnabled( false );
		disabledCodeBatch.setName( "Disabled codeBatch" );
		disabledCodeBatch.setCodeGroup( codeGroup );
		codeBatchRepository.save( disabledCodeBatch );

		codeBatchForDisabledCodeGroup = new CodeBatch();
		codeBatchForDisabledCodeGroup.setEnabled( true );
		codeBatchForDisabledCodeGroup.setName( "CodeBatch in disabled codeGroup" );
		codeBatchForDisabledCodeGroup.setCodeGroup( codeGroupDisabled );
		codeBatchRepository.save( codeBatchForDisabledCodeGroup );

		batchForEndedGroup = new CodeBatch();
		batchForEndedGroup.setEnabled( true );
		batchForEndedGroup.setName( "CodeBatch in ended codeGroup" );
		batchForEndedGroup.setCodeGroup( endedCodeGroup );
		codeBatchRepository.save( batchForEndedGroup );

		futureCodeBatch = new CodeBatch();
		futureCodeBatch.setEnabled( false );
		futureCodeBatch.setName( "futureCodeBatch" );
		futureCodeBatch.setCodeGroup( codeGroup );
		futureCodeBatch.setCodeRuleSet( futureCodeSet );
		codeBatchRepository.save( futureCodeBatch );

		passedEndDateCodeBatch = new CodeBatch();
		passedEndDateCodeBatch.setEnabled( false );
		passedEndDateCodeBatch.setName( "passsendEndDateCodeBatch" );
		passedEndDateCodeBatch.setCodeGroup( codeGroup );
		passedEndDateCodeBatch.setCodeRuleSet( passedEndDateCodeRuleSet );
		codeBatchRepository.save( passedEndDateCodeBatch );

		codeBatchForFutureGroup = new CodeBatch();
		codeBatchForFutureGroup.setEnabled( false );
		codeBatchForFutureGroup.setName( "codeBatchForFutureGroup" );
		codeBatchForFutureGroup.setCodeGroup( futureCodeGroup );
		codeBatchRepository.save( codeBatchForFutureGroup );

	}

	private void createDefaultCodeRuleSets() {
		passedEndDateCodeRuleSetForCodeGroup = new CodeRuleSet();
		passedEndDateCodeRuleSetForCodeGroup.setStartDate( LocalDateTime.now().plusDays( -3 ) );
		passedEndDateCodeRuleSetForCodeGroup.setEndDate( LocalDateTime.now().plusDays( -1 ) );
		passedEndDateCodeRuleSetForCodeGroup.setCodeGroup( endedCodeGroup );
		codeRuleSetRepository.save( passedEndDateCodeRuleSetForCodeGroup );

		threeUsageRuleSet = new CodeRuleSet();
		threeUsageRuleSet.setCodeGroup( codeGroup);
		threeUsageRuleSet.setNumberOfTimesACodeCanBeUsed( 3 );
		threeUsageRuleSet.setNumberOfCodesThatCanBeUsed( 3 );
		codeRuleSetRepository.save( threeUsageRuleSet );

		defaultCodeRuleSet = new CodeRuleSet();
		defaultCodeRuleSet.setCodeGroup( codeGroup );
		defaultCodeRuleSet.setNumberOfTimesACodeCanBeUsed( 1 );
		defaultCodeRuleSet.setNumberOfCodesThatCanBeUsed( 3 );
		codeRuleSetRepository.save( defaultCodeRuleSet );

		rightTimeRuleSet = new CodeRuleSet();
		rightTimeRuleSet.setStartDate( LocalDateTime.now().plusDays( -1 ) );
		rightTimeRuleSet.setEndDate( LocalDateTime.now().plusDays( 1 ) );
		rightTimeRuleSet.setCodeGroup( codeGroup );
		codeRuleSetRepository.save( rightTimeRuleSet );

		passedEndDateCodeRuleSet = new CodeRuleSet();
		passedEndDateCodeRuleSet.setStartDate( LocalDateTime.now().plusDays( -3 ) );
		passedEndDateCodeRuleSet.setEndDate( LocalDateTime.now().plusDays( -1 ) );
		passedEndDateCodeRuleSet.setCodeGroup( codeGroup );
		codeRuleSetRepository.save( passedEndDateCodeRuleSet );

		futureCodeSet = new CodeRuleSet();
		futureCodeSet.setStartDate( LocalDateTime.now().plusDays( 1 ) );
		futureCodeSet.setCodeGroup( codeGroup );
		codeRuleSetRepository.save( futureCodeSet );

	}

	private void createCodeGroups() {
		codeGroup = new CodeGroup();
		codeGroup.setName( "Test code group 1" );
		codeGroup.setEnabled( true );
		codeGroupRepository.save( codeGroup );

		codeGroupDisabled = new CodeGroup();
		codeGroupDisabled.setName( "DisabledCodeGroup" );
		codeGroupDisabled.setEnabled( false );
		codeGroupRepository.save( codeGroupDisabled );

		endedCodeGroup = new CodeGroup();
		endedCodeGroup.setName( "Ended code group" );
		endedCodeGroup.setEnabled( true );
		codeGroupRepository.save( endedCodeGroup );

		futureCodeGroup = new CodeGroup();
		futureCodeGroup.setName( "Future code group" );
		futureCodeGroup.setEnabled( true );
		codeGroupRepository.save( futureCodeGroup );
	}

	protected void assertCodeValue( Code code, String codeValue ) {
		assertEquals( codeValue, code.getValue() );
	}

	protected void assertNoCodeValue( Code code ) {
		assertNull( code );
	}
}
