package it;

import com.foreach.across.modules.codes.domain.code.Code;
import com.foreach.across.modules.codes.services.CodeService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

public class ITFindUsableCodeWithCodeBatch extends AbstractCodeFinderTest
{

	@Test
	public void nothingFoundForInvalidCode() {
		Code code = codeService.findUsableCode( "invalidCode", codeBatchTwo );
		assertNoCodeValue( code );
	}

	@Test
	public void findUsableCodeByCode() {
		Code code = codeService.findUsableCode( "AvailableCode", codeBatchTwo );
		assertCodeValue( code, "AvailableCode" );
	}

	@Test
	public void whenCodeIsAlreadyUsed() {
		Code code = codeService.findUsableCode( "UsedCode", codeBatchTwo );
		assertNoCodeValue( code );
	}

	@Test
	public void withDisabledCodeBatch() {
		Code code = codeService.findUsableCode( "disabled", disabledCodeBatch );
		assertNoCodeValue( code );
	}

	@Test
	public void withDisabledCodeGroup() {
		Code code = codeService.findUsableCode( "disabledCodeGroup", codeBatchForDisabledCodeGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void withStartDateOnCode() {
		Code code = codeService.findUsableCode( "codeInTheFuture", codeBatchTwo );
		assertNoCodeValue( code );
	}

	@Test
	public void withFutureStartDateOnCodeBatch() {
		Code code = codeService.findUsableCode( "futureCodeBatchCode", futureCodeBatch );
		assertNoCodeValue( code );
	}

	@Test
	public void withStartDateOnCodeGroup() {
		Code code = codeService.findUsableCode( "futureCodeGroup", codeBatchForFutureGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void withPassedEndDateOnCode() {
		Code code = codeService.findUsableCode( "CodeWithEndDate", codeBatchTwo );
		assertNoCodeValue( code );
	}

	@Test
	public void withEndDateOnCodeBatch() {
		Code code = codeService.findUsableCode( "endDatePassedCodeGroup", passedEndDateCodeBatch );
		assertNoCodeValue( code );
	}

	@Test
	public void withEndDateOnCodeGroup() {
		Code code = codeService.findUsableCode( "endedCodeGroup", batchForEndedGroup );
		assertNoCodeValue( code );
	}

	@Test
	public void findCodeByCodeWithTwoOuOfTThreeUsedCodes() {
		Code code = codeService.findUsableCode( "UsedCodeTwice", codeBatchTwo );
		assertCodeValue( code, "UsedCodeTwice" );
	}

	@Test
	public void canNotFindCodeByCodeWithThreeOuOfThreeUsedCodes() {
		Code code = codeService.findUsableCode( "CodeUsedThreeTimes", codeBatchTwo );
		assertNoCodeValue( code );
	}

	//TODO: Should be implemented
//	@Test
//	public void canNotFindCodeWithNumberOfCodesThatCanBeUsedReached() {
//		Code code = codeService.findUsableCode( oneOfTheGeneratedCodesThatIsNotUsed.getValue(), codeBatchOne );
//		assertNoCodeValue( code );
//
//		Code code2 = codeService.findUsableCode( oneOfTheGeneratedCodesThatIsUsed.getValue(), codeBatchOne );
//		assertNoCodeValue( code2 );
//	}
}
