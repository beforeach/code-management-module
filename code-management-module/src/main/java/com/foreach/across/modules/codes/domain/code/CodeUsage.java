package com.foreach.across.modules.codes.domain.code;

import com.foreach.across.modules.hibernate.business.SettableIdBasedEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "cmm_code_usage")
@Getter
@Setter
@Entity
@NoArgsConstructor
public class CodeUsage extends SettableIdBasedEntity<CodeUsage>
{
	@Id
	@GeneratedValue(generator = "cmm_seq_code_usage_id")
	@GenericGenerator(
			name = "cmm_seq_code_usage_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "cmm_seq_code_usage_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	@OneToOne
	@JoinColumn(name = "code_id")
	private Code code;

	@Column(name = "usage_date")
	private LocalDateTime usageDate;
}
