package com.foreach.across.modules.codes.domain.code;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Service
@RequiredArgsConstructor
public class CodeRuleSetService
{
	private final CodeRuleSetRepository codeRuleSetRepository;
	private final CodeRepository codeRepository;
	private final CodeBatchRepository codeBatchRepository;
	private final CodeGroupRepository codeGroupRepository;

	/**
	 * Deletes a {@link CodeRuleSet} if there are no codes, batches or groups linking to it.
	 *
	 * @param ruleSet to delete
	 * @return true if ruleset was deleted
	 */
	public boolean deleteRuleSetIfPossible( @NonNull CodeRuleSet ruleSet ) {
		if ( codeGroupRepository.findAllByCodeRuleSet( ruleSet ).isEmpty()
				&& codeBatchRepository.findAllByCodeRuleSet( ruleSet ).isEmpty()
				&& codeRepository.findAllByCodeRuleSet( ruleSet ).isEmpty() ) {
			codeRuleSetRepository.delete( ruleSet );
			return true;
		}
		return false;
	}
}
