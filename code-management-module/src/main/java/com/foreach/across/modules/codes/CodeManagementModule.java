package com.foreach.across.modules.codes;

import com.foreach.across.core.AcrossModule;
import com.foreach.across.core.annotations.AcrossDepends;
import com.foreach.across.core.context.configurer.ApplicationContextConfigurer;
import com.foreach.across.core.context.configurer.ComponentScanConfigurer;

import java.util.Set;

/**
 * THe code management module
 */
@AcrossDepends(
		required = { "AcrossWebModule" },
		optional = { "EntityModule", "AdminWebModule", "FileManagerModule"}
)
public class CodeManagementModule extends AcrossModule
{
	public static final String NAME = "CodeManagementModule";

	@Override
	public String getName() {
		return NAME;
	}

	public String getDescription() {
		return "Provides code management functionality.";
	}

	protected void registerDefaultApplicationContextConfigurers( Set<ApplicationContextConfigurer> contextConfigurers ) {
		contextConfigurers.add( ComponentScanConfigurer.forAcrossModule( CodeManagementModule.class ) );
	}
}
