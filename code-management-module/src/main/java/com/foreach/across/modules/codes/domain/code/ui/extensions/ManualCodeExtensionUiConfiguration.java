package com.foreach.across.modules.codes.domain.code.ui.extensions;

import com.foreach.across.modules.bootstrapui.elements.TextboxFormElement;
import com.foreach.across.modules.codes.config.ConditionalOnAdminUI;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnAdminUI
public class ManualCodeExtensionUiConfiguration implements EntityConfigurer
{
	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.create()
		        .entityType( ManualCodeExtension.class, true )
		        .show()
		        .as( ManualCodeExtension.class )
		        .properties( props -> props
				        .property( "codes[]" )
				        .attribute( TextboxFormElement.Type.class, TextboxFormElement.Type.TEXT )
				        .controller( ctl -> ctl.withTarget( Object.class, String.class )
				                               .contextualValidator( ( o, code, errors, hints ) -> {
					                               if ( StringUtils.isBlank( code ) ) {
						                               errors.rejectValue( "", "NotBlank" );
					                               }
				                               } ) )
		        );
	}
}
