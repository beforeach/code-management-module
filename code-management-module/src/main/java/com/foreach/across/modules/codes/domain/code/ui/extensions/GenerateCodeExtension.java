package com.foreach.across.modules.codes.domain.code.ui.extensions;

import com.foreach.across.modules.codes.domain.code.CodeBatch;
import com.foreach.across.modules.codes.domain.code.CodeRuleSet;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class GenerateCodeExtension
{
	@NotNull
	private CodeBatch codeBatch;
	@NotNull
	private Integer numberOfCodesToGenerate;
	@NotBlank
	@Length(max = 200)
	private String codePattern;
	private CodeRuleSet codeRuleSet;
}
