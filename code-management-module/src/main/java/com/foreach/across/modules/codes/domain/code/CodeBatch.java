package com.foreach.across.modules.codes.domain.code;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * A {@link CodeBatch} that contains a collection of {@link Code}
 */
@Table(name = "cmm_code_batch")
@Getter
@Setter
@Entity
@NoArgsConstructor
public class CodeBatch extends SettableIdAuditableEntity<CodeBatch> implements CodeRuleSetAware
{
	@Id
	@GeneratedValue(generator = "seq_cmm_code_batch_id")
	@GenericGenerator(
			name = "seq_cmm_code_batch_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "seq_cmm_code_batch_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	/**
	 * THe {@link CodeGroup} linked to this {@link CodeGroup}
	 */
	@NotNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "code_group_id")
	private CodeGroup codeGroup;

	/**
	 * The name of the {@link CodeBatch}
	 */
	@Column(name = "name")
	@Length(max = 255)
	@NotBlank
	private String name;

	/**
	 * A boolean indicating if the {@link CodeBatch} is enabled
	 */
	@Column(name = "enabled")
	private boolean enabled = true;

	/**
	 * A {@link CodeRuleSet} linked to this {@link CodeBatch}.
	 * When the {@link CodeRuleSet} is not set, the {@link CodeRuleSet} from the {@link CodeGroup} is used.
	 * See also {@link #getApplicableCodeRuleSet()}.
	 */
	@ManyToOne
	@JoinColumn(name = "code_rule_set_id")
	private CodeRuleSet codeRuleSet;

	/**
	 * @return the rule set that should be applied to this code batch (may be inherited)
	 */
	public CodeRuleSet getApplicableCodeRuleSet() {
		if ( codeRuleSet != null ) {
			return codeRuleSet;
		}
		else {
			return getCodeGroup().getCodeRuleSet();
		}
	}
}