package com.foreach.across.modules.codes.domain.code.ui;

import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.codes.config.ConditionalOnAdminUI;
import com.foreach.across.modules.codes.domain.code.CodeBatch;
import com.foreach.across.modules.codes.domain.code.CodeGroup;
import com.foreach.across.modules.codes.domain.code.ui.elements.CodeRuleSetForm;
import com.foreach.across.modules.codes.domain.code.ui.elements.CodeRuleSetFormController;
import com.foreach.across.modules.codes.domain.code.ui.viewprocessor.CodeRuleSetAwareRowHighlighter;
import com.foreach.across.modules.entity.conditionals.ConditionalOnBootstrapUI;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.config.builders.EntityConfigurationBuilder;
import com.foreach.across.modules.entity.registry.EntityAssociation;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.modules.web.ui.elements.support.ContainerViewElementUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Configuration
@ConditionalOnAdminUI
@ConditionalOnBootstrapUI
@OrderInModule(2)
@RequiredArgsConstructor
public class CodeBatchUiConfiguration implements EntityConfigurer
{
	private final CodeRuleSetFormController ruleSetFormController;

	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.withType( CodeBatch.class )
		        .properties( props -> props.property( "codeRuleSet" ).writable( false ) )
		        .and( ruleSetFormController.registerProperty( CodeRuleSetForm::from ) );

		configureBatchAssociationOnGroup( entities );
	}

	private EntityConfigurationBuilder<CodeGroup> configureBatchAssociationOnGroup( EntitiesConfigurationBuilder entities ) {
		return entities.withType( CodeGroup.class )
		               .association(
				               as -> as.name( "codeBatch.codeGroup" )
				                       .associationType( EntityAssociation.Type.EMBEDDED )
				                       .parentDeleteMode( EntityAssociation.ParentDeleteMode.WARN )
				                       .listView(
						                       lvb -> lvb.showProperties( EntityPropertySelector.CONFIGURED, "~codeRuleSet" )
						                                 .viewProcessor( CodeRuleSetAwareRowHighlighter.INSTANCE )
				                       )
				                       .createOrUpdateFormView( fvb -> fvb.viewProcessor( new EntityViewProcessorAdapter()
				                       {
					                       @Override
					                       protected void postRender( EntityViewRequest entityViewRequest,
					                                                  EntityView entityView,
					                                                  ContainerViewElement container,
					                                                  ViewElementBuilderContext builderContext ) {
						                       ContainerViewElementUtils.move( container, "codeRuleSetForm", SingleEntityFormViewProcessor.RIGHT_COLUMN );
					                       }
				                       } ) )
		               );
	}
}
