package com.foreach.across.modules.codes.services;

import com.foreach.across.modules.codes.domain.code.*;
import com.foreach.across.modules.codes.utils.CodeGenerator;
import com.foreach.across.modules.codes.utils.CodePatternFormatter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class CodeServiceImpl implements CodeService
{
	private final CodeRepository codeRepository;
	private final CodeBatchRepository codeBatchRepository;
	private final CodeUsageRepository codeUsageRepository;

	@Override
	public void generateUniqueCodesBatch( CodeBatch codeBatch, int numberOfCodesToGenerate, String codePattern ) {
		codeBatchRepository.save( codeBatch );

		CodePatternFormatter codePatternReplacer = CodePatternFormatter.forPattern( codePattern );
		CodeGenerator codeGenerator = CodeGenerator.forCodeLength( codePatternReplacer.getCodeLength() );
		buildCodesFrom(
				codeGenerator.generate( numberOfCodesToGenerate ),
				codeBatch,
				true,
				codePatternReplacer
		);
	}

	@Override
	public void createCodes( CodeBatch codeBatch, Collection<String> codes ) {
		buildCodesFrom( codes.iterator(), codeBatch, false );
	}

	@Override
	public void uploadCsvCodes( CodeBatch codeBatch, File file ) throws IOException {
		try (Stream<String> stream = Files.lines( file.toPath() )) {
			buildCodesFrom( stream.iterator(), codeBatch, false );
		}
	}

	@Override
	public void validateCodePattern( String codePattern ) {
		CodePatternFormatter.forPattern( codePattern );
	}

	@Override
	public Code findUsableCode( String code ) {
		List<Code> foundCodes = codeRepository.findAllByValue( code );

		if ( foundCodes.size() < 1 ) {
			return null;
		}

		return filterCodes( foundCodes );
	}

	@Override
	public Code findUsableCode( String code, CodeBatch codeBatch ) {
		List<Code> foundCodes = codeRepository.findAllByValue( code ).stream()
		                                      .filter( codeInCodeBatch -> codeInCodeBatch.getCodeBatch().equals( codeBatch ) )
		                                      .collect( Collectors.toList() );
		if ( foundCodes.size() < 1 ) {
			return null;
		}

		return filterCodes( foundCodes );
	}

	@Override
	public Code findUsableCode( String code, CodeGroup codeGroup ) {
		List<Code> foundCodes = codeRepository.findAllByValue( code ).stream()
		                                      .filter( codeInCodeBatch -> codeInCodeBatch.getCodeBatch().getCodeGroup().equals( codeGroup ) )
		                                      .collect( Collectors.toList() );

		if ( foundCodes.size() < 1 ) {
			return null;
		}

		return filterCodes( foundCodes );
	}

	@Override
	public CodeUsage useCode( Code code ) {
		CodeUsage codeUsage = new CodeUsage();
		codeUsage.setUsageDate( LocalDateTime.now() );
		codeUsage.setCode( code );
		codeUsageRepository.save( codeUsage );
		return codeUsage;
	}

	private Code filterCodes( List<Code> foundCodes ) {
		return foundCodes.stream()
		                 .filter( foundCode ->
				                          foundCode.getCodeBatch().isEnabled() &&
						                          foundCode.getCodeBatch().getCodeGroup().isEnabled() &&
						                          foundCode.getCodeRuleSet() != null &&
						                          ( foundCode.getCodeRuleSet().getStartDate() == null || foundCode.getCodeRuleSet().getStartDate().isBefore(
								                          LocalDateTime.now() ) ) &&
						                          ( foundCode.getCodeRuleSet().getEndDate() == null || foundCode.getCodeRuleSet().getEndDate().isAfter(
								                          LocalDateTime.now() ) ) &&
						                          ( foundCode.getCodeRuleSet().getNumberOfTimesACodeCanBeUsed() == null || codeUsageRepository.findAllByCode(
								                          foundCode ).size() < foundCode.getCodeRuleSet().getNumberOfTimesACodeCanBeUsed() )
//						                          && ( foundCode.getCodeRuleSet().getNumberOfCodesThatCanBeUsed() == null  )
		                 )
		                 .findFirst()
		                 .orElse( null );
	}

	private String prefixCodeForUniqueness( CodeBatch codeBatch, String codeString ) {
		return codeBatch.getId() + codeString;
	}

	private void buildCodesFrom( Iterator<String> codes, CodeBatch codeBatch, Boolean usePrefix ) {
		buildCodesFrom( codes, codeBatch, usePrefix, null );
	}

	private void buildCodesFrom( Iterator<String> codes, CodeBatch codeBatch, Boolean usePrefix, CodePatternFormatter codePatternReplacer ) {
		codes.forEachRemaining( codeString -> {
			Code code = new Code();
			codeString = codePatternReplacer == null ? codeString : codePatternReplacer.replace( codeString );
			code.setValue( usePrefix ? prefixCodeForUniqueness( codeBatch, codeString ) : codeString );
			code.setCodeBatch( codeBatch );
			codeRepository.save( code );
		} );
	}
}
