package com.foreach.across.modules.codes.elements;

import com.foreach.across.modules.web.ui.elements.AbstractNodeViewElement;

public class ButtonDropdownViewElement extends AbstractNodeViewElement
{
	public ButtonDropdownViewElement() {
		super( "div" );
		this.setAttribute( "class", "btn-group" );
	}
}
