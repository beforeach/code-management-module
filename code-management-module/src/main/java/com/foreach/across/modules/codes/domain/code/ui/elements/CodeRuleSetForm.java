package com.foreach.across.modules.codes.domain.code.ui.elements;

import com.foreach.across.modules.codes.domain.code.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.Valid;

@Data
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class CodeRuleSetForm
{
	public static final String PROPERTY_NAME = "rules";

	private final CodeGroup group;
	private final CodeBatch batch;
	private final Code code;

	private boolean inherited = true;

	@Valid
	private CodeRuleSet ruleSet = new CodeRuleSet();

	public CodeRuleSet getRuleSet() {
		if ( ruleSet.getCodeGroup() == null ) {
			ruleSet.setCodeGroup( resolveGroup() );
		}
		return ruleSet;
	}

	private CodeGroup resolveGroup() {
		if ( group != null ) {
			return group;
		}
		if ( batch != null ) {
			return batch.getCodeGroup();
		}
		if ( code != null ) {
			CodeBatch b = code.getCodeBatch();
			if ( b != null ) {
				return b.getCodeGroup();
			}
		}
		return null;
	}

	public static CodeRuleSetForm from( Code code ) {
		CodeRuleSetForm form = new CodeRuleSetForm( null, null, code );
		//transferRuleSet( code, form );
		return form;
	}

	public static CodeRuleSetForm from( CodeBatch batch ) {
		CodeRuleSetForm form = new CodeRuleSetForm( null, batch, null );
		transferRuleSet( batch, form );
		return form;
	}

	public static CodeRuleSetForm from( CodeGroup group ) {
		CodeRuleSetForm form = new CodeRuleSetForm( group, null, null );
		transferRuleSet( group, form );
		return form;
	}

	private static void transferRuleSet( CodeRuleSetAware owner, CodeRuleSetForm form ) {
		if ( owner.getCodeRuleSet() != null ) {
			form.setInherited( false );
			form.setRuleSet( owner.getCodeRuleSet().toDto() );
		}
	}
}
