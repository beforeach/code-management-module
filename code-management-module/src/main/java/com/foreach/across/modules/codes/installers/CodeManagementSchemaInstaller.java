package com.foreach.across.modules.codes.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.installers.AcrossLiquibaseInstaller;
import org.springframework.core.annotation.Order;

/**
 * The schema installer
 *
 * @author Marc Vanbrabant
 * @since 0.0.1
 */
@Order(1)
@Installer(description = "Installs the required database tables", version = 6)
public class CodeManagementSchemaInstaller extends AcrossLiquibaseInstaller
{
	public CodeManagementSchemaInstaller() {
		super( "installers/CodeManagementModule/schema.xml" );
	}
}
