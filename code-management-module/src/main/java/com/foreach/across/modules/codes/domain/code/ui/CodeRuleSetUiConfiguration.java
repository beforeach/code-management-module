package com.foreach.across.modules.codes.domain.code.ui;

import com.foreach.across.modules.codes.config.ConditionalOnAdminUI;
import com.foreach.across.modules.codes.domain.code.CodeRuleSet;
import com.foreach.across.modules.codes.domain.code.CodeRuleSetValueFetcher;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnAdminUI
@RequiredArgsConstructor
public class CodeRuleSetUiConfiguration implements EntityConfigurer
{

	private final CodeRuleSetValueFetcher codeRuleSetValueFetcher;

	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.withType( CodeRuleSet.class )
		        .properties(
				        props -> props.label().valueFetcher( codeRuleSetValueFetcher ).and()
				                      .property( "codeGroup" ).hidden( true )
		        );
	}
}
