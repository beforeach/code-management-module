package com.foreach.across.modules.codes.domain.code;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The repository for the {@link CodeGroup}
 */
@Repository
public interface CodeGroupRepository extends IdBasedEntityJpaRepository<CodeGroup>
{
	List<CodeGroup> findAllByCodeRuleSet( CodeRuleSet ruleSet );
}
