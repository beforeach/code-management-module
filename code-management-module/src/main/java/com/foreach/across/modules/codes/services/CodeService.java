package com.foreach.across.modules.codes.services;

import com.foreach.across.modules.codes.domain.code.Code;
import com.foreach.across.modules.codes.domain.code.CodeBatch;
import com.foreach.across.modules.codes.domain.code.CodeGroup;
import com.foreach.across.modules.codes.domain.code.CodeUsage;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

public interface CodeService
{
	/**
	 * Add a list of {@link Code} the the provided {@link CodeBatch} ;
	 * The value of the {@link Code} are created using the {@link com.foreach.across.modules.codes.utils.CodeGenerator}
	 *  @param codeBatch that holds the the created {@link Code}
	 * @param numberOfCodesToGenerate the number of codes that will be generated
	 * @param codePattern A pattern in format {@code {4}-XYZ-{10}-BCD} which generates code based on this format
	 */
	void generateUniqueCodesBatch( CodeBatch codeBatch, int numberOfCodesToGenerate, String codePattern );

	/**
	 * Create and save a list of {@link Code} from a collections of Strings
	 *
	 * @param codeBatch
	 * @param codes
	 */
	void createCodes( CodeBatch codeBatch, Collection<String> codes );

	/**
	 * Populate the {@link CodeBatch} with {@link Code} from the csv file
	 *
	 * @param codeBatch
	 * @param file
	 * @throws IOException
	 */
	public void uploadCsvCodes( CodeBatch codeBatch, File file ) throws IOException;

	public Code findUsableCode( String code );

	public Code findUsableCode( String code, CodeBatch codeBatch );

	public Code findUsableCode( String code, CodeGroup codeGroup );

	public CodeUsage useCode(Code code);

	void validateCodePattern( String codePattern );
}
