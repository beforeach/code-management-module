package com.foreach.across.modules.codes.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.modules.hibernate.installers.AuditableSchemaInstaller;
import org.springframework.core.annotation.Order;

import java.util.Arrays;
import java.util.Collection;

/**
 * Installer that adds auditable columns
 */
@Order(2)
@Installer(description = "Adds auditing columns to core tables", version = 1)
public class CodeManagementAuditableInstaller extends AuditableSchemaInstaller
{
	@Override
	protected Collection<String> getTableNames() {
		return Arrays.asList( "cmm_code", "cmm_code_group", "cmm_code_batch", "cmm_code_rule_set" );
	}
}
