package com.foreach.across.modules.codes.domain.code.ui.viewprocessor;

import com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders;
import com.foreach.across.modules.codes.builders.ButtonDropdownViewElementBuilder;
import com.foreach.across.modules.codes.domain.code.ui.events.ButtonDropDownEvent;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.context.EntityViewContext;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.modules.web.ui.elements.NodeViewElement;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ButtonDropdownViewProcessor extends EntityViewProcessorAdapter
{
	private final ApplicationEventPublisher eventPublisher;

	@Override
	protected void postRender( EntityViewRequest entityViewRequest,
	                           EntityView entityView,
	                           ContainerViewElement container,
	                           ViewElementBuilderContext builderContext ) {
		EntityViewContext entityViewContext = entityViewRequest.getEntityViewContext();

		ButtonDropdownViewElementBuilder buttonDropdownViewElementBuilder = new ButtonDropdownViewElementBuilder()
				.text( "Add codes" )
				.buttonClass( "btn-primary" )
				.link( BootstrapUiBuilders.link().url(
						entityViewContext.getLinkBuilder().createView()
						                 .withViewName( "manual" )
						                 .toUriString() )
				                          .text( "Add manual code" )
				)
				.link( BootstrapUiBuilders.link().url(
						entityViewContext.getLinkBuilder().createView()
						                 .withViewName( "generate" )
						                 .toUriString() )
				                          .text( "Generate codes" )
				);

		ButtonDropDownEvent buttonDropDownEvent = new ButtonDropDownEvent();
		buttonDropDownEvent.setButtonDropdownViewElementBuilder( buttonDropdownViewElementBuilder );

		eventPublisher.publishEvent( buttonDropDownEvent );

		container.find( "entityListForm-actions", NodeViewElement.class )
		         .ifPresent( actions -> {
			         actions.removeFromTree( "btn-create" );

			         actions.addChild(
					         buttonDropDownEvent.getButtonDropdownViewElementBuilder().build()
			         );
		         } );
	}
}
