package com.foreach.across.modules.codes.config;

import com.foreach.across.modules.web.events.BuildMenuEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
@RequiredArgsConstructor
public class AdminMenuConfiguration
{
	@EventListener
	public void buildSideNavigation( BuildMenuEvent menu ) {
		menu.item( "/entities/CodeManagementModule/code" ).disable()
		    .and().item( "/entities/CodeManagementModule/codeBatch" ).disable()
		    .and().item( "/entities/CodeManagementModule/codeRuleSet" ).disable()
		    .and().item( "/entities/CodeManagementModule/codeUsage" ).disable()
		    .and().item( "/entities/generateCodeExtension" ).disable()
		    .and().item( "/entities/manualCodeExtension" ).disable()
		    .and().item( "/entities/uploadCodeExtension" ).disable()
		    .disable();
	}
}
