package com.foreach.across.modules.codes.domain.code.ui.elements;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders;
import com.foreach.across.modules.bootstrapui.elements.builder.OptionsFormElementBuilder;
import com.foreach.across.modules.codes.domain.code.CodeRuleSet;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.entity.bind.EntityPropertyControlName;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderHelper;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.elements.form.dependencies.Qualifiers;
import com.foreach.across.modules.entity.views.helpers.EntityViewElementBatch;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.web.ui.ViewElementBuilder;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.NodeViewElement;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders.*;

/**
 * Renders the controls for a {@link CodeRuleSetForm}.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@ConditionalOnAcrossModule("EntityModule")
@RequiredArgsConstructor
public class CodeRuleSetFormBuilder implements ViewElementBuilder<NodeViewElement>
{
	private final EntityViewElementBuilderHelper builderHelper;

	@Override
	public NodeViewElement build( ViewElementBuilderContext builderContext ) {
		CodeRuleSetForm rules = EntityViewElementUtils.currentPropertyValue( builderContext, CodeRuleSetForm.class );

		EntityPropertyControlName controlName = EntityViewElementUtils.controlName(
				EntityViewElementUtils.currentPropertyDescriptor( builderContext ), builderContext
		);

		EntityViewElementBatch<CodeRuleSet> form = builderHelper.createBatchForEntity( rules.getRuleSet() );
		form.setViewElementMode( ViewElementMode.FORM_WRITE );
		form.setPropertySelector( EntityPropertySelector.of( "*" ) );
		form.setParentViewElementBuilderContext( builderContext );
		form.setAttribute( EntityPropertyControlName.class, controlName.forChildProperty( "ruleSet" ) );

		String radioControlName = controlName.forChildProperty( "inherited" ).toString();
		OptionsFormElementBuilder radio = options()
				.controlName( radioControlName )
				.radio()
				.add( BootstrapUiBuilders.option().value( true ).label( "inherited" ).selected( rules.isInherited() ) )
				.add( BootstrapUiBuilders.option().htmlId( "code-rules-specific" ).value( false ).label( "specific rules" ).selected( !rules.isInherited() ) );

		Qualifiers q = new Qualifiers();
		q.setValues( Collections.singletonList( "false" ) );

		Map<String, Object> config = new HashMap<>();
		config.put( "input[name='" + radioControlName + "']", q );
		config.put( "options", Collections.singletonMap( "hide", true ) );
		return div()
				.name( "codeRuleSetForm" )
				.css( "panel", "panel-default" )
				.add( div().css( "panel-heading" ).add( text( builderContext.getMessage( "validation.rules" ) ) ) )
				.add(
						div().css( "panel-body" )
						     .add( radio )
						     .add(
								     div().htmlId( "rules-form" )
								          .data( "dependson", config )
								          .addAll( form.build().values() )
						     )
				)
				.build( builderContext );
	}
}
