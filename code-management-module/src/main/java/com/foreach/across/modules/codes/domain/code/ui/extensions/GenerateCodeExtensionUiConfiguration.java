package com.foreach.across.modules.codes.domain.code.ui.extensions;

import com.foreach.across.modules.codes.config.ConditionalOnAdminUI;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnAdminUI
public class GenerateCodeExtensionUiConfiguration implements EntityConfigurer
{
	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.create()
		        .entityType(GenerateCodeExtension.class, true)
		        .show()
		        .as(GenerateCodeExtension.class);
	}
}
