package com.foreach.across.modules.codes.domain.code;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The repository for the {@link CodeBatch}
 */
@Repository
public interface CodeBatchRepository extends IdBasedEntityJpaRepository<CodeBatch>
{
	List<CodeBatch> findAllByCodeGroup( CodeGroup codeGroup );

	List<CodeBatch> findAllByCodeRuleSet( CodeRuleSet ruleSet );
}
