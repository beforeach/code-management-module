package com.foreach.across.modules.codes.domain.code.ui;

import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiElements;
import com.foreach.across.modules.codes.config.ConditionalOnAdminUI;
import com.foreach.across.modules.codes.domain.code.CodeGroup;
import com.foreach.across.modules.codes.domain.code.ui.elements.CodeRuleSetForm;
import com.foreach.across.modules.codes.domain.code.ui.elements.CodeRuleSetFormBuilder;
import com.foreach.across.modules.codes.domain.code.ui.elements.CodeRuleSetFormController;
import com.foreach.across.modules.codes.domain.code.ui.viewprocessor.CodeRuleSetViewProcessor;
import com.foreach.across.modules.codes.domain.code.ui.viewprocessor.CodeRuleSetAwareRowHighlighter;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.conditionals.ConditionalOnBootstrapUI;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.registry.EntityAssociation;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.modules.web.ui.elements.support.ContainerViewElementUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;

@Configuration
@ConditionalOnAdminUI
@ConditionalOnBootstrapUI
@OrderInModule(1)
@RequiredArgsConstructor
public class CodeGroupUiConfiguration implements EntityConfigurer
{
	private final CodeRuleSetViewProcessor codeRuleSetViewProcessor;
	private final CodeRuleSetFormBuilder codeRuleSetFormBuilder;
	private final CodeRuleSetFormController ruleSetFormController;

	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.create()
		        .entityType( CodeRuleSetForm.class, true )
		        .viewElementBuilder( ViewElementMode.FORM_WRITE, codeRuleSetFormBuilder );

		entities.withType( CodeGroup.class )
		        .properties( props -> props.property( "codeRuleSet" ).writable( false ) )
		        .and( ruleSetFormController.registerProperty( CodeRuleSetForm::from ) )
		        .createOrUpdateFormView( fvb -> fvb.viewProcessor( new EntityViewProcessorAdapter()
		        {
			        @Override
			        protected void postRender( EntityViewRequest entityViewRequest,
			                                   EntityView entityView,
			                                   ContainerViewElement container,
			                                   ViewElementBuilderContext builderContext ) {
				        ContainerViewElementUtils.move( container, "codeRuleSetForm", SingleEntityFormViewProcessor.RIGHT_COLUMN );
			        }
		        } ) )
		        .listView(
				        lvb -> lvb.showProperties( EntityPropertySelector.CONFIGURED, "~codeRuleSet" )
				                  .defaultSort( new Sort( Sort.Direction.ASC, "name" ) )
				                  .entityQueryFilter( eqf -> eqf.showProperties( "name", "enabled" ) )
				                  .pageSize( 25 )
				                  .viewProcessor( CodeRuleSetAwareRowHighlighter.INSTANCE )
		        )
		     /*   .updateFormView(
				        ufb -> ufb.properties(
						        props -> props.property( "codeRuleSet" )
						                      .displayName( "Rule Set" )
						                      .viewElementType( ViewElementMode.FORM_WRITE, BootstrapUiElements.FIELDSET )
						                      .attribute(
								                      EntityAttributes.FIELDSET_PROPERTY_SELECTOR,
								                      EntityPropertySelector.of( "codeRuleSet.*", "~codeRuleSet.codeGroup" )
						                      )

				        )
				                  .viewProcessor( codeRuleSetViewProcessor )
		        )*/
		        .association(
				        as -> as.name( "codeRuleSet.codeGroup" )
				                .associationType( EntityAssociation.Type.EMBEDDED )
		        );
	}
}
