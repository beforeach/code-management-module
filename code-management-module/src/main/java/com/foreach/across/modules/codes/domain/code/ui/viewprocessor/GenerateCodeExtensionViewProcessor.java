package com.foreach.across.modules.codes.domain.code.ui.viewprocessor;

import com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders;
import com.foreach.across.modules.codes.config.ConditionalOnAdminUI;
import com.foreach.across.modules.codes.domain.code.ui.extensions.GenerateCodeExtension;
import com.foreach.across.modules.codes.services.CodeService;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderHelper;
import com.foreach.across.modules.entity.views.helpers.EntityViewElementBatch;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

@Component
@ConditionalOnAdminUI
public class GenerateCodeExtensionViewProcessor extends BaseExtensionViewProcessor<GenerateCodeExtension>
{
	private final CodeService codeService;

	public GenerateCodeExtensionViewProcessor( EntityViewElementBuilderHelper entityViewElementBuilderHelper,
	                                           CodeService codeService ) {
		super( entityViewElementBuilderHelper );
		this.codeService = codeService;
	}

	@Override
	protected void doPost( EntityViewElementBatch<GenerateCodeExtension> extension,
	                       BindingResult bindingResult,
	                       EntityView entityView,
	                       EntityViewRequest entityViewRequest ) {
		GenerateCodeExtension generateCodeExtension = (GenerateCodeExtension) extension.getPropertiesBinder().getEntity();
		extension.getPropertiesBinder().createController().applyValues();

		if ( !bindingResult.hasErrors() ) {
			codeService.generateUniqueCodesBatch(
					generateCodeExtension.getCodeBatch(),
					generateCodeExtension.getNumberOfCodesToGenerate(),
					generateCodeExtension.getCodePattern()
			);
			entityViewRequest.getPageContentStructure()
			                 .addToFeedback(
					                 BootstrapUiBuilders.alert()
					                                    .success()
					                                    .dismissible()
					                                    .text( "Codes have been created " )
					                                    .build()
			                 );
		}
	}

	@Override
	protected void validateExtension( EntityViewElementBatch<GenerateCodeExtension> extension,
	                                  Errors errors,
	                                  HttpMethod httpMethod,
	                                  EntityViewRequest entityViewRequest ) {
		if ( HttpMethod.POST.equals( httpMethod ) ) {
			errors.popNestedPath();
			if ( !errors.hasFieldErrors( "entity.codePattern" ) ) {
				GenerateCodeExtension generateCodeExtension = (GenerateCodeExtension) extension.getPropertiesBinder().getEntity();
				try {
					codeService.validateCodePattern( generateCodeExtension.getCodePattern() );
				}
				catch ( RuntimeException e ) {
					errors.rejectValue( "entity.codePattern", "parsingFailed", new String[] { e.getMessage() }, "Error parsing pattern." );
				}
			}
			errors.pushNestedPath( controlPrefix() );
		}
	}
}
