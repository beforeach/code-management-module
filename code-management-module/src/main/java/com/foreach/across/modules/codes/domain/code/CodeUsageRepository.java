package com.foreach.across.modules.codes.domain.code;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The repository for the {@link CodeUsage}
 */
@Repository
public interface CodeUsageRepository extends IdBasedEntityJpaRepository<CodeUsage>
{
	List<CodeUsage> findAllByCode(Code code);
}
