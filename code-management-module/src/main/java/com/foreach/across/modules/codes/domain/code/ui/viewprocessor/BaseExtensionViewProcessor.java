package com.foreach.across.modules.codes.domain.code.ui.viewprocessor;

import com.foreach.across.modules.bootstrapui.elements.builder.ColumnViewElementBuilder;
import com.foreach.across.modules.codes.config.ConditionalOnAdminUI;
import com.foreach.across.modules.entity.bind.EntityPropertyControlName;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderHelper;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.helpers.EntityViewElementBatch;
import com.foreach.across.modules.entity.views.processors.ExtensionViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.processors.support.ViewElementBuilderMap;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.web.ui.ViewElement;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.builder.ContainerViewElementBuilderSupport;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.WebDataBinder;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

@ConditionalOnAdminUI
@RequiredArgsConstructor
public class BaseExtensionViewProcessor<T> extends ExtensionViewProcessorAdapter<EntityViewElementBatch<T>>
{
	private final EntityViewElementBuilderHelper entityViewElementBuilderHelper;
	@Setter
	protected EntityPropertySelector entityPropertySelector = EntityPropertySelector.of( "*" );

	@SuppressWarnings("unchecked")
	public Class<T> getTypeParameterClass() {
		Type type = getClass().getGenericSuperclass();
		ParameterizedType paramType = (ParameterizedType) type;
		return (Class<T>) paramType.getActualTypeArguments()[0];
	}

	@Override
	protected EntityViewElementBatch<T> createExtension( EntityViewRequest entityViewRequest, EntityViewCommand command, WebDataBinder dataBinder ) {
		// Create a new batch for the extension
		T entity = null;
		try {
			entity = getTypeParameterClass().newInstance();
		}
		catch ( InstantiationException e ) {
			e.printStackTrace();
		}
		catch ( IllegalAccessException e ) {
			e.printStackTrace();
		}
		EntityViewElementBatch<T> batch = entityViewElementBuilderHelper.createBatchForEntity( entity );

		// Configure the propertiesBinder
		// Add the extension as target to apply values later on
		batch.getPropertiesBinder().setTarget( entity );
		batch.getPropertiesBinder().setBinderPrefix( "properties" );

		// Set the entity & propertiesBinder on the command object for validation
		command.setEntity( batch.getPropertiesBinder().getEntity() );
		command.setProperties( batch.getPropertiesBinder() );

		// Mostly for lists, from this point on the binder expects values for a list
		// So if no values are provided form this point on, the list will explicitly be set to empty
		if ( HttpMethod.POST.equals( entityViewRequest.getHttpMethod() ) ) {
			batch.getPropertiesBinder().setBindingEnabled( true );
		}

		return batch;
	}

	@Override
	protected void render( EntityViewElementBatch<T> batch,
	                       EntityViewRequest entityViewRequest,
	                       EntityView entityView,
	                       ContainerViewElementBuilderSupport<?, ?> containerBuilder,
	                       ViewElementBuilderMap builderMap,
	                       ViewElementBuilderContext builderContext ) {

		batch.setViewElementMode( ViewElementMode.FORM_WRITE );
		batch.setPropertySelector( entityPropertySelector );
		batch.setAttribute( EntityPropertyControlName.class,
		                    EntityPropertyControlName.root( "entity" ) );

		Map<String, ViewElement> fieldsByName = batch.build();

		ColumnViewElementBuilder builder = builderMap.get( SingleEntityFormViewProcessor.LEFT_COLUMN, ColumnViewElementBuilder.class );

		fieldsByName.forEach(
				( name, elem ) -> builder.add( elem )
		);
	}
}
