package com.foreach.across.modules.codes.domain.code;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The repository for the {@link CodeRuleSet}
 */
@Repository
public interface CodeRuleSetRepository extends IdBasedEntityJpaRepository<CodeRuleSet>
{
}
