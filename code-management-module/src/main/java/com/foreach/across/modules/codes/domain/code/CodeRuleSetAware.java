package com.foreach.across.modules.codes.domain.code;

public interface CodeRuleSetAware
{
	void setCodeRuleSet( CodeRuleSet codeRuleSet );

	CodeRuleSet getCodeRuleSet();

	void setEnabled( boolean enabled );

	boolean isEnabled();
}
