package com.foreach.across.modules.codes.domain.code.ui.extensions;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.codes.domain.code.CodeBatch;
import com.foreach.across.modules.codes.domain.code.CodeRuleSet;
import com.foreach.across.modules.filemanager.business.reference.FileReference;
import lombok.Data;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Data
@ConditionalOnAcrossModule("FileManagerModule")
public class UploadCodeExtension
{
	@NotNull
	private CodeBatch codeBatch;
	@NotNull
	@ManyToOne
	private FileReference codes;
	private CodeRuleSet codeRuleSet;
}
