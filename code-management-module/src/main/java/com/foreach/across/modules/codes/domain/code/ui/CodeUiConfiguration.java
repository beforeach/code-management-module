package com.foreach.across.modules.codes.domain.code.ui;

import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.codes.config.ConditionalOnAdminUI;
import com.foreach.across.modules.codes.domain.code.Code;
import com.foreach.across.modules.codes.domain.code.CodeGroup;
import com.foreach.across.modules.codes.domain.code.ui.viewprocessor.ButtonDropdownViewProcessor;
import com.foreach.across.modules.codes.domain.code.ui.viewprocessor.GenerateCodeExtensionViewProcessor;
import com.foreach.across.modules.codes.domain.code.ui.viewprocessor.ManualCodeExtensionViewProcessor;
import com.foreach.across.modules.codes.services.CodeService;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.query.AssociatedEntityQueryExecutor;
import com.foreach.across.modules.entity.query.EntityQueryExecutor;
import com.foreach.across.modules.entity.registry.EntityAssociation;
import com.foreach.across.modules.entity.registry.EntityFactory;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyController;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.EntityViewCustomizers;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.TypeDescriptor;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Configures the codes association on {@link CodeGroup}.
 */
@Configuration
@OrderInModule(50)
@ConditionalOnAdminUI
@RequiredArgsConstructor
public class CodeUiConfiguration implements EntityConfigurer
{
	private final CodeService codeService;
	private final ButtonDropdownViewProcessor buttonDropdownViewProcessor;
	private final GenerateCodeExtensionViewProcessor generateCodeExtensionViewProcessor;
	private final ManualCodeExtensionViewProcessor manualCodeExtensionViewProcessor;

	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.withType( CodeGroup.class )
		        .association(
				        ab -> ab.name( "codes" )
				                .targetEntityType( Code.class )
				                .associationType( EntityAssociation.Type.EMBEDDED )
				                .targetProperty( "codeBatch.codeGroup" )
				                .show()
				                .listView( lvb -> lvb
						                .viewProcessor( buttonDropdownViewProcessor )
						                .entityQueryFilter( eqf -> eqf.showProperties( "value", "codeBatch" ) )
						                .properties(
								                props -> props.property( "codeBatch" )
						                )
				                )
				                .detailView()
				                .deleteFormView()
				                .createFormView( fvb -> fvb
						                .properties(
								                props -> props
										                .property( "codeValues" )
										                .propertyType(
												                TypeDescriptor.collection( Collection.class, TypeDescriptor.valueOf( String.class ) ) )
										                .hidden( false )
										                .writable( true )
										                .controller( ctl -> ctl.withTarget( Code.class, Collection.class )
										                                       .order( EntityPropertyController.BEFORE_ENTITY )
										                                       .saveConsumer( ( code, codeValues ) -> {
											                                       if ( codeValues.getNewValue() != null && code.getCodeBatch() != null ) {
												                                       codeService.createCodes(
														                                       code.getCodeBatch(),
														                                       new ArrayList<String>( codeValues.getNewValue() ) );
											                                       }
										                                       } )
										                )
						                )
						                .showProperties( EntityPropertySelector.CONFIGURED, "~value" )
				                )
				                .updateFormView()
				                .formView(
						                "generate",
						                EntityViewCustomizers.basicSettings()
						                                     .andThen( EntityViewCustomizers.formSettings().forExtension( true ) )
						                                     .andThen( builder -> builder.viewProcessor( generateCodeExtensionViewProcessor ) )
				                )
				                .formView(
						                "manual",
						                EntityViewCustomizers.basicSettings()
						                                     .andThen( EntityViewCustomizers.formSettings().forExtension( true ) )
						                                     .andThen( builder -> builder.viewProcessor( manualCodeExtensionViewProcessor ) )
				                )
				                .attribute( EntityFactory.class, (
						                new EntityFactory<Code>()
						                {
							                @Override
							                public Code createNew( Object... args ) {
								                CodeGroup codeGroup = (CodeGroup) args[0];
								                Code code = new Code();
								                return code;
							                }

							                @Override
							                public Code createDto( Code entity ) {
								                return entity;
							                }
						                } ) )
				                .attribute( ( association, writableAttributes ) -> {
					                            writableAttributes.setAttribute(
							                            AssociatedEntityQueryExecutor.class,
							                            new AssociatedEntityQueryExecutor(
									                            association.getTargetProperty(),
									                            association.getTargetEntityConfiguration().getAttribute( EntityQueryExecutor.class )
							                            )
					                            );
				                            }
				                )
		        );
	}
}
