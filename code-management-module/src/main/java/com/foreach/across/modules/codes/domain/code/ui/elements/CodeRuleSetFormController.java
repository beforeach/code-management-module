package com.foreach.across.modules.codes.domain.code.ui.elements;

import com.foreach.across.modules.codes.domain.code.*;
import com.foreach.across.modules.entity.config.builders.EntityConfigurationBuilder;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyController;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.SmartValidator;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class CodeRuleSetFormController
{
	private final SmartValidator entityValidator;
	private final CodeRuleSetRepository codeRuleSetRepository;
	private final CodeRuleSetService codeRuleSetService;
	private final CodeGroupRepository codeGroupRepository;
	private final CodeRepository codeRepository;
	private final CodeBatchRepository codeBatchRepository;

	@SuppressWarnings("unchecked")
	public <V> Consumer<EntityConfigurationBuilder<V>> registerProperty( Function<V, CodeRuleSetForm> factory ) {
		return cfg -> {
			cfg.properties( props -> props
					.property( "rules" )
					.propertyType( CodeRuleSetForm.class )
					.readable( true )
					.writable( true )
					.hidden( false )
					.controller(
							ctl -> ctl.withTarget( CodeRuleSetAware.class, CodeRuleSetForm.class )
							          .order( EntityPropertyController.AFTER_ENTITY )
							          .valueFetcher( (Function<CodeRuleSetAware, CodeRuleSetForm>) factory )
							          /*.applyValueConsumer( ( target, val ) -> {
								          if ( target instanceof CodeGroup ) {
									          applyValue( (CodeGroup) target, val.getNewValue() );
								          }
							          } )
							          */
							          .saveConsumer( ( target, val ) -> {
								          save( target, val.getNewValue() );

//								          if ( target instanceof CodeGroup ) {
//									          save( (CodeGroup) target, val.getNewValue() );
//								          }
//								          else {
//									          save( val.getNewValue() );
//								          }
							          } )
							          .contextualValidator( ( object, form, errors, hints ) -> {
								          if ( !form.isInherited() ) {
									          // only apply validation if validation settings are not inherited
									          entityValidator.validate( form, errors, hints );
								          }
							          } )
					)
			);
		};
	}

	void save( CodeRuleSetAware owner, CodeRuleSetForm form ) {
		CodeRuleSet ruleSet = form.getRuleSet();

		boolean shouldDelete = form.isInherited() && !ruleSet.isNew();

		if ( !form.isInherited() ) {
			codeRuleSetRepository.save( ruleSet );

			owner.setCodeRuleSet( ruleSet );
			save( owner );
		}
		else if ( shouldDelete ) {
			owner.setCodeRuleSet( null );
			save( owner );
			codeRuleSetService.deleteRuleSetIfPossible( ruleSet );
		}
	}

	private void save( CodeRuleSetAware owner ) {
		if ( owner instanceof CodeGroup ) {
			codeGroupRepository.save( (CodeGroup) owner );
		}
		else if ( owner instanceof CodeBatch ) {
			codeBatchRepository.save( (CodeBatch) owner );
		}
	}

	void save( CodeGroup group, CodeRuleSetForm form ) {
		CodeRuleSet ruleSet = form.getRuleSet();

		boolean shouldDelete = form.isInherited() && !ruleSet.isNew();

		if ( !form.isInherited() ) {
			codeRuleSetRepository.save( ruleSet );

			group.setCodeRuleSet( ruleSet );
			codeGroupRepository.save( group );
		}
		else if ( shouldDelete ) {
			group.setCodeRuleSet( null );
			codeGroupRepository.save( group );
			codeRuleSetService.deleteRuleSetIfPossible( ruleSet );
		}
	}

	void applyValue( CodeGroup group, CodeRuleSetForm form ) {
		if ( form.isInherited() ) {
			group.setCodeRuleSet( null );
		}
		/*
		else {
			group.setCodeRuleSet( form.getRuleSet() );
		}*/
	}

	private void save( CodeRuleSetForm form ) {
		CodeRuleSet ruleSet = form.getRuleSet();
		if ( form.isInherited() ) {
			// todo: if there are no more other direct links to this ruleset, delete it
			if ( !ruleSet.isNew() ) {
				codeRuleSetRepository.delete( ruleSet );
			}
		}
		else {
			codeRuleSetRepository.save( ruleSet );
		}
	}
}
