package com.foreach.across.modules.codes.builders;

import com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders;
import com.foreach.across.modules.bootstrapui.elements.builder.AbstractLinkSupportingNodeViewElementBuilder;
import com.foreach.across.modules.bootstrapui.elements.builder.LinkViewElementBuilder;
import com.foreach.across.modules.codes.elements.ButtonDropdownViewElement;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.NodeViewElement;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

public class ButtonDropdownViewElementBuilder extends AbstractLinkSupportingNodeViewElementBuilder<ButtonDropdownViewElement, ButtonDropdownViewElementBuilder>
{
	private List<LinkViewElementBuilder> links = new ArrayList<>();
	private String text;
	private String buttonClass;

	/**
	 * Adds a links to the dropdown menu of the button
	 *
	 * @param link
	 * @return this
	 */
	public ButtonDropdownViewElementBuilder link( LinkViewElementBuilder link ) {
		this.links.add( link );
		return this;
	}

	/**
	 * The text shown on the dropdown button
	 *
	 * @param text
	 * @return this
	 */
	public ButtonDropdownViewElementBuilder text( String text ) {
		this.text = text;
		return this;
	}

	/**
	 * Add a class to the button of the button group
	 *
	 * @param buttonClass to add to the button
	 * @return this
	 */
	public ButtonDropdownViewElementBuilder buttonClass( String buttonClass ) {
		this.buttonClass = buttonClass;
		return this;
	}

	@Override
	protected ButtonDropdownViewElement createElement( ViewElementBuilderContext builderContext ) {
		ButtonDropdownViewElement buttonDropdown = new ButtonDropdownViewElement();

		buttonDropdown.addChild( BootstrapUiBuilders.button()
		                                            .text( addCaretToText() )
		                                            .attribute( "class", "dropdown-toggle " + buttonClass )
		                                            .attribute( "data-toggle", "dropdown" )
		                                            .attribute( "aria-haspopup", "true" )
		                                            .attribute( "aria-expanded=", "false" )
		                                            .build() );
		buttonDropdown.addChild( buildDropdownLinks( builderContext ) );
		return apply( buttonDropdown, builderContext );
	}

	private String addCaretToText() {
		return text + " <span class=\"caret\"></span>";
	}

	protected NodeViewElement buildDropdownLinks( ViewElementBuilderContext builderContext ) {
		NodeViewElement list = BootstrapUiBuilders.node( "ul" ).build( builderContext );
		list.addCssClass( "dropdown-menu" );

		links.forEach( link -> {
			NodeViewElement li = new NodeViewElement( "li" );
			li.addChild( link.build( builderContext ) );
			list.addChild( li );
		} );

		return list;
	}
}
