package com.foreach.across.modules.codes.config;

import com.foreach.across.modules.codes.domain.DomainMarker;
import com.foreach.across.modules.hibernate.jpa.repositories.config.EnableAcrossJpaRepositories;
import org.springframework.context.annotation.Configuration;

/**
 * @author Marc Vanbrabant
 * @since 0.0.1
 */
@Configuration
@EnableAcrossJpaRepositories(basePackageClasses = DomainMarker.class)
public class CodeManagementDomainConfiguration
{
}
