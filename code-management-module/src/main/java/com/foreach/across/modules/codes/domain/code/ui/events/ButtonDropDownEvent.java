package com.foreach.across.modules.codes.domain.code.ui.events;

import com.foreach.across.core.events.NamedAcrossEvent;
import com.foreach.across.modules.codes.builders.ButtonDropdownViewElementBuilder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ButtonDropDownEvent implements NamedAcrossEvent
{
	private ButtonDropdownViewElementBuilder buttonDropdownViewElementBuilder;

	@Override
	public String getEventName() {
		return "ButtonDropDown";
	}
}
