package com.foreach.across.modules.codes.domain.code.ui.viewprocessor;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders;
import com.foreach.across.modules.bootstrapui.elements.FormViewElement;
import com.foreach.across.modules.codes.config.ConditionalOnAdminUI;
import com.foreach.across.modules.codes.domain.code.ui.extensions.UploadCodeExtension;
import com.foreach.across.modules.codes.services.CodeService;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderHelper;
import com.foreach.across.modules.entity.views.helpers.EntityViewElementBatch;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.filemanager.services.FileManager;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.io.IOException;

@Component
@ConditionalOnAdminUI
@ConditionalOnAcrossModule("FileManagerModule")
public class UploadCodeExtensionViewProcessor extends BaseExtensionViewProcessor<UploadCodeExtension>
{
	private final CodeService codeService;
	private final FileManager fileManager;

	public UploadCodeExtensionViewProcessor( EntityViewElementBuilderHelper entityViewElementBuilderHelper,
	                                         FileManager fileManager,
	                                         CodeService codeService ) {
		super( entityViewElementBuilderHelper );
		this.codeService = codeService;
		this.fileManager = fileManager;
	}

	@Override
	protected void postRender( EntityViewElementBatch<UploadCodeExtension> extension,
	                           EntityViewRequest entityViewRequest,
	                           EntityView entityView,
	                           ContainerViewElement container,
	                           ViewElementBuilderContext builderContext ) {
		container.find( "entityForm", FormViewElement.class )
		         .ifPresent( form -> form.setAttribute( "enctype", FormViewElement.ENCTYPE_MULTIPART ) );
	}

	@Override
	protected void doPost( EntityViewElementBatch<UploadCodeExtension> batch,
	                       BindingResult bindingResult,
	                       EntityView entityView,
	                       EntityViewRequest entityViewRequest ) {
		UploadCodeExtension uploadCodeExtension = (UploadCodeExtension) batch.getPropertiesBinder().getEntity();

		if ( !bindingResult.hasErrors() ) {
			try {
				codeService.uploadCsvCodes(
						uploadCodeExtension.getCodeBatch(),
						fileManager.getAsFile( uploadCodeExtension.getCodes().getFileDescriptor() )
				);
			}
			catch ( IOException e ) {
				e.printStackTrace();
			}

			entityViewRequest.getPageContentStructure()
			                 .addToFeedback(
					                 BootstrapUiBuilders.alert()
					                                    .success()
					                                    .dismissible()
					                                    .text( "Codes have been created" )
					                                    .build()
			                 );
		}
	}
}
