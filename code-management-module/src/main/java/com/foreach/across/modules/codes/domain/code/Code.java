package com.foreach.across.modules.codes.domain.code;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * A {@link Code} that contains the value, the linked {@link CodeBatch} and an optional {@link CodeRuleSet}
 */
@Table(name = "cmm_code")
@Entity
@Setter
@Getter
@NoArgsConstructor
// todo: make CodeRuleSetAware
public class Code extends SettableIdAuditableEntity<Code>
{
	@Id
	@GeneratedValue(generator = "seq_cmm_code_id")
	@GenericGenerator(
			name = "seq_cmm_code_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "seq_cmm_code_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	/**
	 * The {@link CodeBatch} linked to the code
	 */
	@NotNull
	@ManyToOne
	@JoinColumn(name = "code_batch_id")
	private CodeBatch codeBatch;

	/**
	 * The value representing the code
	 */
	@Column(name = "code_value")
	@Length(max = 255)
	private String value;

	/**
	 * An optional {@link CodeRuleSet} mapped to this code.
	 * When no {@link CodeRuleSet} has been set, the {@link CodeRuleSet} of the {@link CodeBatch} will be used
	 */
	@ManyToOne
	@JoinColumn(name = "code_rule_set_id")
	private CodeRuleSet codeRuleSet;

	/**
	 * @return the rule set that should be applied to this code (may be inherited)
	 */
	public CodeRuleSet getApplicableCodeRuleSet() {
		if ( codeRuleSet != null ) {
			return codeRuleSet;
		}
		else {
			return getCodeBatch().getCodeRuleSet();
		}
	}
}
