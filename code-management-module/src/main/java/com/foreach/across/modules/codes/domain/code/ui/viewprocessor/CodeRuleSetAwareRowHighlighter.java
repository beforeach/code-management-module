package com.foreach.across.modules.codes.domain.code.ui.viewprocessor;

import com.foreach.across.modules.bootstrapui.elements.Style;
import com.foreach.across.modules.bootstrapui.elements.TableViewElement;
import com.foreach.across.modules.codes.domain.code.CodeRuleSetAware;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.bootstrapui.util.SortableTableBuilder;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SortableTableRenderingViewProcessor;
import com.foreach.across.modules.entity.views.processors.support.ViewElementBuilderMap;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.builder.ContainerViewElementBuilderSupport;

/**
 * Add appropriate row highlighting to a {@link CodeRuleSetAware} entity, based on the
 * value of {@link CodeRuleSetAware#isEnabled()}.
 *
 * todo: different highlighting depending on the actual ruleset validity
 */
public class CodeRuleSetAwareRowHighlighter extends EntityViewProcessorAdapter
{
	public static final CodeRuleSetAwareRowHighlighter INSTANCE = new CodeRuleSetAwareRowHighlighter();

	@Override
	protected void render( EntityViewRequest entityViewRequest,
	                       EntityView entityView,
	                       ContainerViewElementBuilderSupport<?, ?> containerBuilder,
	                       ViewElementBuilderMap builderMap,
	                       ViewElementBuilderContext builderContext ) {
		SortableTableBuilder tableBuilder = builderMap.get( SortableTableRenderingViewProcessor.TABLE_BUILDER, SortableTableBuilder.class );
		tableBuilder.valueRowProcessor( this::highlightRowBasedOnEnabled );
	}

	private void highlightRowBasedOnEnabled( ViewElementBuilderContext builderContext, TableViewElement.Row row ) {
		CodeRuleSetAware codeRuleSetAware = EntityViewElementUtils.currentEntity( builderContext, CodeRuleSetAware.class );
		if ( codeRuleSetAware.isEnabled() ) {
			row.addCssClass( Style.SUCCESS.getName() );
		}
		else {
			row.addCssClass( Style.DANGER.getName() );
		}
	}
}
