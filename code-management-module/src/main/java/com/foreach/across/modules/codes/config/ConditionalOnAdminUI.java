package com.foreach.across.modules.codes.config;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;

import java.lang.annotation.*;

/**
 * Helper conditional annotation for the combination of AdminWebModule and EntityModule.
 *
 * @author Stijn Vanhoof
 * @since 0.0.1
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@ConditionalOnClass(EntityConfigurer.class)
@ConditionalOnAcrossModule(allOf = { AdminWebModule.NAME, EntityModule.NAME })
@Inherited
public @interface ConditionalOnAdminUI
{
}