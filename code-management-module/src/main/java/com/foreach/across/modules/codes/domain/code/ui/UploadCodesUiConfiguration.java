package com.foreach.across.modules.codes.domain.code.ui;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders;
import com.foreach.across.modules.bootstrapui.elements.FormViewElement;
import com.foreach.across.modules.codes.domain.code.CodeGroup;
import com.foreach.across.modules.codes.domain.code.ui.events.ButtonDropDownEvent;
import com.foreach.across.modules.codes.domain.code.ui.viewprocessor.UploadCodeExtensionViewProcessor;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.conditionals.ConditionalOnBootstrapUI;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.views.EntityViewCustomizers;
import com.foreach.across.modules.entity.views.context.EntityViewContext;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@OrderInModule(100)
@Configuration
@RequiredArgsConstructor
@ConditionalOnBootstrapUI
@ConditionalOnAcrossModule("FileManagerModule")
public class UploadCodesUiConfiguration implements EntityConfigurer
{
	private final UploadCodeExtensionViewProcessor uploadCodeExtensionViewProcessor;
	private final EntityViewContext entityViewContext;

	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.withType( CodeGroup.class )
		        .association(
				        ab -> ab.name( "codes" )
				                .formView(
						                "upload",
						                EntityViewCustomizers.basicSettings()
						                                     .andThen( EntityViewCustomizers.formSettings().forExtension( true ) )
						                                     .andThen( builder -> builder.viewProcessor( uploadCodeExtensionViewProcessor )
						                                     .attribute( EntityAttributes.FORM_ENCTYPE, FormViewElement.ENCTYPE_MULTIPART )
						                                     )

				                )
		        );
	}

	@EventListener(value = ButtonDropDownEvent.class)
	public void addUploadCsvLinkButton(ButtonDropDownEvent buttonDropDownEvent){
		buttonDropDownEvent.getButtonDropdownViewElementBuilder()
		                   .link(
				                   BootstrapUiBuilders.link()
				                                      .url( entityViewContext.getLinkBuilder().createView()
				                                          .withViewName( "upload" )
				                                          .toUriString() )
				                                      .text( "Upload codes" )
		                   );
	}
}

