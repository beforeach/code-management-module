package com.foreach.across.modules.codes.domain.code;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The repository for the {@link Code}
 */
@Repository
public interface CodeRepository extends IdBasedEntityJpaRepository<Code>
{
	List<Code> findAllByValue( String code );

	List<Code> findAllByValueAndCodeBatch( String code, CodeBatch codeBatch );

	List<Code> findAllByCodeRuleSet( CodeRuleSet ruleSet );
}
