package com.foreach.across.modules.codes.domain.code.ui.viewprocessor;

import com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders;
import com.foreach.across.modules.codes.config.ConditionalOnAdminUI;
import com.foreach.across.modules.codes.domain.code.ui.extensions.ManualCodeExtension;
import com.foreach.across.modules.codes.services.CodeService;
import com.foreach.across.modules.entity.conditionals.ConditionalOnBootstrapUI;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderHelper;
import com.foreach.across.modules.entity.views.helpers.EntityViewElementBatch;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

@Component
@ConditionalOnAdminUI
@ConditionalOnBootstrapUI
public class ManualCodeExtensionViewProcessor extends BaseExtensionViewProcessor<ManualCodeExtension>
{
	private final CodeService codeService;

	public ManualCodeExtensionViewProcessor( EntityViewElementBuilderHelper entityViewElementBuilderHelper,
	                                         CodeService codeService ) {
		super( entityViewElementBuilderHelper );
		this.codeService = codeService;
	}

	@Override
	protected void doPost( EntityViewElementBatch<ManualCodeExtension> batch,
	                       BindingResult bindingResult,
	                       EntityView entityView,
	                       EntityViewRequest entityViewRequest ) {
		ManualCodeExtension manualCodeExtension = (ManualCodeExtension) batch.getPropertiesBinder().getEntity();

		if ( !bindingResult.hasErrors() ) {
			codeService.createCodes(
					manualCodeExtension.getCodeBatch(),
					manualCodeExtension.getCodes()
			);

			entityViewRequest.getPageContentStructure()
			                 .addToFeedback(
					                 BootstrapUiBuilders.alert()
					                                    .success()
					                                    .dismissible()
					                                    .text( "Codes have been created " )
					                                    .build()
			                 );
		}
	}

}
