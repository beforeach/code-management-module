package com.foreach.across.modules.codes.domain;

/**
 * A simple marker interface to use for scanning the domain models.
 *
 * @author Marc Vanbrabant
 * @since 0.0.1
 */
public interface DomainMarker
{
}
