package com.foreach.across.modules.codes.domain.code.ui.extensions;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.bootstrapui.elements.FormViewElement;
import com.foreach.across.modules.codes.config.ConditionalOnAdminUI;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnAdminUI
@ConditionalOnAcrossModule("FileManagerModule")
public class UploadCodeExtensionUiConfiguration implements EntityConfigurer
{
	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.create()
		        .entityType(UploadCodeExtension.class, true)
		        .show()
		        .as(UploadCodeExtension.class);
	}
}
