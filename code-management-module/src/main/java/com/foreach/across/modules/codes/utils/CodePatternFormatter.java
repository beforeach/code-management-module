package com.foreach.across.modules.codes.utils;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@AllArgsConstructor
public class CodePatternFormatter
{
	private static final Pattern PATTERN = Pattern.compile( "\\{(\\d+)\\}" );
	private final List<PatternInfo> patternInfos;
	private final String pattern;

	public static CodePatternFormatter forPattern( String codePattern ) {
		CodePatternFormatter formatter = new CodePatternFormatter( retrieveCodeLengthForPattern( codePattern ), codePattern );
		int codeLength = formatter.getCodeLength();
		if ( codeLength <= 0 ) {
			throw new RuntimeException( "Invalid pattern: \"" + codePattern + "\". Must be in format ABC-{14}-XYZ." );
		}
		if ( codeLength > 10000 ) {
			throw new RuntimeException(
					"The total length of codes cannot exceed 10000. The current code length for pattern \"" + codePattern + "\" is \"" + codeLength + "\"." );
		}
		return formatter;
	}

	public int getCodeLength() {
		return patternInfos.stream().mapToInt( PatternInfo::getLength ).sum();
	}

	private static List<PatternInfo> retrieveCodeLengthForPattern( String codePattern ) {
		Matcher matcher = PATTERN.matcher( codePattern );
		int length;
		int startIndex = 0;
		int endIndex;
		List<PatternInfo> patternInfo = new ArrayList<>();
		while ( matcher.find() ) {
			String group = matcher.group( 0 );
			NumberUtils.isParsable( StringUtils.replaceChars( matcher.group( 0 ), "{}", "" ) );
			length = NumberUtils.toInt( StringUtils.replaceChars( matcher.group( 0 ), "{}", "" ), -1 );
			if ( length < 0 ) {
				throw new RuntimeException( "The pattern argument must be less than: \"" + Integer.MAX_VALUE + "\"." );
			}
			endIndex = startIndex + length;
			patternInfo.add( new PatternInfo( group, startIndex, endIndex ) );
			startIndex = endIndex;
		}
		return patternInfo;
	}

	public String replace( String code ) {
		String formattedCode = pattern;
		for ( PatternInfo patternInfo : patternInfos ) {
			String codePart = StringUtils.substring( code, patternInfo.startIndex, patternInfo.endIndex );
			formattedCode = StringUtils.replaceOnce( formattedCode, patternInfo.group, codePart );
		}
		return formattedCode;
	}

	@AllArgsConstructor
	private static class PatternInfo
	{
		private String group;
		private int startIndex;
		private int endIndex;

		public int getLength() {
			return endIndex - startIndex;
		}
	}
}
