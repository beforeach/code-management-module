package com.foreach.across.modules.codes.domain.code.ui.extensions;

import com.foreach.across.modules.codes.domain.code.CodeBatch;
import com.foreach.across.modules.codes.domain.code.CodeRuleSet;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

@Data
public class ManualCodeExtension
{
	@NotNull
	private CodeBatch codeBatch;
	@NotEmpty
	private List<String> codes;
	private CodeRuleSet codeRuleSet;
}
