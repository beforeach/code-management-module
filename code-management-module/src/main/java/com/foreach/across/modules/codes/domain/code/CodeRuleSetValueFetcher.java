package com.foreach.across.modules.codes.domain.code;

import com.foreach.across.modules.entity.views.support.ValueFetcher;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class CodeRuleSetValueFetcher implements ValueFetcher<CodeRuleSet> {
    private final MessageSource messageSource;

    @Override
    public Object getValue( CodeRuleSet entity ) {
        if( entity.getEndDate() != null && entity.getStartDate() != null ) {
            Date start = localDateToDate( entity.getStartDate() );
            Date end = localDateToDate( entity.getEndDate() );
            return messageSource.getMessage( "CodeManagementModule.codeRuleSet.valueFetcher.between", new Object[]{start, end}, LocaleContextHolder.getLocale() );
        }
        if( entity.getEndDate() != null ) {
            Date end = localDateToDate( entity.getEndDate() );
            return messageSource.getMessage( "CodeManagementModule.codeRuleSet.valueFetcher.endDate", new Object[]{end}, LocaleContextHolder.getLocale() );
        }
        if( entity.getStartDate() != null ) {
            Date start = localDateToDate( entity.getStartDate() );
            return messageSource.getMessage( "CodeManagementModule.codeRuleSet.valueFetcher.endDate", new Object[]{start}, LocaleContextHolder.getLocale() );
        }
        if( entity.getNumberOfTimesACodeCanBeUsed() != null ) {
            return messageSource.getMessage( "CodeManagementModule.codeRuleSet.valueFetcher.limitPerCode", new Object[]{entity.getNumberOfTimesACodeCanBeUsed()}, LocaleContextHolder.getLocale() );
        }
        if( entity.getNumberOfCodesThatCanBeUsed() != null ) {
            return messageSource.getMessage( "CodeManagementModule.codeRuleSet.valueFetcher.limitPerCode", new Object[]{entity.getNumberOfCodesThatCanBeUsed()}, LocaleContextHolder.getLocale() );
        }
        return null;
    }

    private Date localDateToDate( LocalDateTime endDate ) {
        return Date.from( endDate.toLocalDate().atStartOfDay()
                .atZone( ZoneId.systemDefault() )
                .toInstant() );
    }
}
