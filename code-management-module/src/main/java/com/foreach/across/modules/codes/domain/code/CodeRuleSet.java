package com.foreach.across.modules.codes.domain.code;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Table( name = "cmm_code_rule_set" )
@Getter
@Setter
@Entity
@NoArgsConstructor
public class CodeRuleSet extends SettableIdAuditableEntity<CodeRuleSet> {
    @Id
    @GeneratedValue( generator = "cmm_seq_code_rule_set_id" )
    @GenericGenerator(
            name = "cmm_seq_code_rule_set_id",
            strategy = AcrossSequenceGenerator.STRATEGY,
            parameters = {
                    @org.hibernate.annotations.Parameter( name = "sequenceName", value = "cmm_seq_code_rule_set_id" ),
                    @org.hibernate.annotations.Parameter( name = "allocationSize", value = "1" )
            }
    )
    private Long id;

    @Column( name = "start_date" )
    private LocalDateTime startDate;

    @Column( name = "end_date" )
    private LocalDateTime endDate;

    /**
     * A number indicating how many times a single code can be used
     * Setting this to {@code null} indicates the code can be used an unlimited time
     */
    @Min( value = 1 )
    @Column( name = "times_a_code_can_be_used" )
    private Integer numberOfTimesACodeCanBeUsed;

    /**
     * A number indicating howMany codes of this batch can be used
     * Setting this to {@code null} indicates there is no limit on the codes that can be used for this batch
     */
    @Min( value = 1 )
    @Column( name = "number_of_codes_can_be_used" )
    private Integer numberOfCodesThatCanBeUsed;

    /**
     * The codegroup in which the ruleset can be applied.
     */
    @NotNull
    @ManyToOne
    @JoinColumn( name = "code_group_id" )
    private CodeGroup codeGroup;
}
