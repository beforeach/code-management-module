package com.foreach.across.modules.codes.domain.code;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

/**
 * A {@link CodeGroup} consists of one or more {@link CodeBatch} entities.
 * This {@link CodeGroup} has a name, can be enabled and has an optional {@link CodeRuleSet}
 */
@Table(name = "cmm_code_group")
@Getter
@Setter
@Entity
@NoArgsConstructor
public class CodeGroup extends SettableIdAuditableEntity<CodeGroup> implements CodeRuleSetAware
{
	@Id
	@GeneratedValue(generator = "cmm_seq_code_group_id")
	@GenericGenerator(
			name = "cmm_seq_code_group_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "cmm_seq_code_group_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	/**
	 * The {@link CodeGroup} name
	 */
	@NotBlank
	@Length(max = 255)
	@Column(name = "name")
	private String name;

	/**
	 * A boolean that indicates if the {@link CodeGroup} was enabled
	 */
	@Column(name = "enabled")
	private boolean enabled = true;

	/**
	 * A {@link CodeRuleSet} for the whole {@link CodeGroup}
	 */
	@ManyToOne
	@JoinColumn(name = "code_rule_set_id")
	private CodeRuleSet codeRuleSet;
}
