package com.foreach.across.samples.code.test;

import com.foreach.across.config.AcrossApplication;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.codes.CodeManagementModule;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.filemanager.FileManagerModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.properties.PropertiesModule;
import org.springframework.boot.SpringApplication;

@AcrossApplication(
		modules = {
				CodeManagementModule.NAME,
				AdminWebModule.NAME,
				EntityModule.NAME,
				AcrossHibernateJpaModule.NAME,
				FileManagerModule.NAME,
				PropertiesModule.NAME
		}
)
public class CodeManagementTestApplication
{
	public static void main( String[] args ) {
		SpringApplication.run( CodeManagementTestApplication.class, args );
	}
}



